﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using AnalyticToolAddData.Models;
using ExcelDataReader;

namespace AnalyticToolAddData.Parsers
{
    public class XslxParser
    {
        public static void ParseWorkerSkillsFromXlsx(ICollection<WorkerSkills> workersSkills, string filePath)
        {
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet();
                    DataRow firstRow = result.Tables[0].Rows[0];
                    for (var i = 1; i < result.Tables[0].Rows.Count; i++)
                    {
                        var workerSkills = ParseToWorkerSkills(firstRow, result.Tables[0].Rows[i]);
                        workersSkills.Add(workerSkills);
                    }

                }
            }
        }

        public static void ParseUsersFromXlsx(ICollection<Worker> workers, string filePath)
        {
            using (var stream = File.Open(filePath, FileMode.Open, FileAccess.Read))
            {
                using (var reader = ExcelReaderFactory.CreateReader(stream))
                {
                    var result = reader.AsDataSet();
                    var props = typeof(Worker).GetProperties();
                    for (var i = 1; i < result.Tables[1].Rows.Count; i++)
                    {
                        var worker = new Worker();
                        for (var j = 0; j < result.Tables[1].Rows[i].ItemArray.Length; j++)
                        {
                            props[j].SetValue(worker, result.Tables[1].Rows[i][j].ToString());
                        }
                        if (worker.Department.Contains("SEDO"))
                        {
                            worker.Department = "SEDO";
                        }

                        workers.Add(worker);
                    }
                }
            }
        }

        private static WorkerSkills ParseToWorkerSkills(DataRow firstRow, DataRow row)
        {
            var skills = new WorkerSkills
            {
                Name = row[0].ToString(),
                Message = row[1].ToString(),
                ItemType = row[row.ItemArray.Length - 2].ToString(),
                Path = row[row.ItemArray.Length - 1].ToString(),
                Skills = new List<KeyValuePair<Skill, int>>()
            };

            for (var i = 2; i < row.ItemArray.Length - 2; i++)
            {
                skills.Skills.Add(new KeyValuePair<Skill, int>(ParseToWorkerSkills((string) firstRow[i]),
                    ParseLevel(row[i].ToString())));
            }

            return skills;
        }

        static Skill ParseToWorkerSkills(string text)
        {
            return new Skill
            {
                Name = text.Split('_')[1],
                Type = text.Split('_')[0].Remove(0, text.IndexOf('.') + 1).Trim()
            };
        }

        static int ParseLevel(string text)
        {
            switch (text)
            {
                case "N/A":
                case "":
                    return 0;
                default:
                    return Int32.Parse(text);
            }
        }

    }
}