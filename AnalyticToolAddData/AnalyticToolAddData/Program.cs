﻿using System;
using System.Collections.Generic;
using AnalyticToolAddData.Models;
using AnalyticToolAddData.DataTransfer;
using System.Linq;
using AnalyticToolAddData.Parsers;

namespace AnalyticToolAddData
{
    class Program
    {
        static void Main(string[] args)
        {
            var workers = new List<Worker>();
            XslxParser.ParseUsersFromXlsx(workers, "..\\..\\Files\\peopleAndSkills.xlsx");

            var skills = new List<WorkerSkills>();
            XslxParser.ParseWorkerSkillsFromXlsx(skills, "..\\..\\Files\\peopleAndSkills.xlsx");

            //send users
            var personManager = new PersonManager();
           // personManager.SendData(workers).Wait();

            //send user details
            var personDetailsManager = new PersonDetailsManager();
           personDetailsManager.SendData(workers).Wait();

            //send skill types
            var skillTypeManager = new SkillTypeManager();
            skillTypeManager.SendData(skills[0].Skills
                .GroupBy(s => s.Key.Type)
                .Select(skill => skill.First().Key)
                .ToList()).Wait();

            ////send skills
            var skillManager = new SkillManager();
            skillManager.SendData(skills[0].Skills
                .Select(s => s.Key)
                .ToList()).Wait();

            ////send person skills
            var personSkillsManager = new PersonSkillsManager();
            personSkillsManager.SendData(skills).Wait();

            Console.ReadKey();
        }
    }
}
