﻿using System.Collections.Generic;

namespace AnalyticToolAddData.Models
{
    public class EntityItems
    {
        public ICollection<EntityItem> Items { get; set; }
        public string Count { get; set; }
        public string IsChangable { get; set; }

    }
}
