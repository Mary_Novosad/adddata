﻿namespace AnalyticToolAddData.Models
{
    public class Worker
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string DisplayTitle { get; set; }
        public string Superior { get; set; }
        public string Room { get; set; }
        public string FirstNameUk { get; set; }
        public string JobTitle { get; set; }
        public string Region { get; set; }
        public string Project { get; set; }
        public string Position { get; set; }
        public string Department { get; set; }
        public string Division { get; set; }
        public string JobType { get; set; }
        public string ProfessionalArea { get; set; }
        public string Login { get; set; }
        public string EleksEmail { get; set; }
        public string Disabled { get; set; }
        public string LeaveStatus { get; set; }
        public string Place { get; set; }
        public string Phone { get; set; }
        public string Cellphone { get; set; }
        public string SkypeMessenger { get; set; }
        public string Gender { get; set; }
        public string EnglishLevel { get; set; }
        public string CarModelName { get; set; }
        public string RegistrationNumber { get; set; }
        public string Day { get; set; }
        public string Month { get; set; }
    }
}
