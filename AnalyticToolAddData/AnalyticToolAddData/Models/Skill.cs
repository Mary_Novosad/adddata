﻿namespace AnalyticToolAddData.Models
{
    public class Skill
    {
        public string Name { get; set; }
        public string Type { get; set; }
    }
}
