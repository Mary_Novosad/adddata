﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AnalyticToolAddData.Models.DTOs
{
    public class PersonDetails
    {
        public long Id { get; set; }
        public long People { get; set; }
        public string Login { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Skype { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EnglishLevel { get; set; }
        public string TechnicalLevel { get; set; }
        public string JobTitle { get; set; }

    }
}
