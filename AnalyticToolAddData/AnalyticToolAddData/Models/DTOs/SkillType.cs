﻿namespace AnalyticToolAddData.Models.DTOs
{
    public class SkillType
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
