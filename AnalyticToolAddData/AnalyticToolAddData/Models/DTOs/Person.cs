﻿namespace AnalyticToolAddData.Models.DTOs
{
    public class Person
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public bool? IsActive {get; set; }
        public long Location { get; set; }
        public long Department { get; set; }
    }
}
