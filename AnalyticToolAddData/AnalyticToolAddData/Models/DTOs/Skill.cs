﻿namespace AnalyticToolAddData.Models.DTOs
{
    public class Skill
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long? SuperiorSkill { get; set; }
        public long SkillType { get; set; }
        public int FadeoutPeriod { get; set; }
        public string LearningSource { get; set; }
    }
}
