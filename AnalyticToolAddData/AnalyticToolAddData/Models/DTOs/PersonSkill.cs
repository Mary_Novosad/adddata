﻿namespace AnalyticToolAddData.Models.DTOs
{
    public class PersonSkill
    {
        public long Id { get; set; }
        public long People { get; set; }
        public long Skill { get; set; }
        public int Level { get; set; }

    }
}
