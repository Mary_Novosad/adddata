﻿using System.Collections.Generic;

namespace AnalyticToolAddData.Models
{
    public class WorkerSkills
    {
        public string Name { get; set; }
        public string Message { get; set; }
        //Skill - skill, int - level
        public ICollection<KeyValuePair<Skill, int>> Skills { get; set; }
        public string ItemType { get; set; }
        public string Path { get; set; }
    }
}
