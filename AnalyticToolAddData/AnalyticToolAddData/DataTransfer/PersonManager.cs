﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnalyticToolAddData.Models;
using AnalyticToolAddData.Models.DTOs;

namespace AnalyticToolAddData.DataTransfer
{
    public class PersonManager
    {
        private const string RequestPath = "http://localhost:55569/api/data/";
        private void ChangeRelationFields(List<Worker> workers, EntityItems persons, EntityItems locations, EntityItems departments)
        {
            workers.ForEach(worker =>
            {
                EntityItem person = persons.Items.FirstOrDefault(item => item["name"].Value.ToString().Equals(worker.FullName, StringComparison.InvariantCultureIgnoreCase));
                worker.Id = person?["id"].Value.ToString();
                EntityItem location = locations.Items.FirstOrDefault(item => item["name"].Value.ToString().Equals(worker.Region, StringComparison.InvariantCultureIgnoreCase));
                worker.Region = location["id"].Value.ToString();
                EntityItem department = departments.Items.FirstOrDefault(item => item["name"].Value.ToString().Equals(worker.Department, StringComparison.InvariantCultureIgnoreCase));
                worker.Department = department["id"].Value.ToString();
            });
        }

        private Person FormPersonToSend(Worker worker, EntityItems persons)
        {
            long id = 0;
            Int64.TryParse(worker.Id, out id);
            var existingPerson = persons.Items.FirstOrDefault(item => item["id"].Value.ToString() == worker.Id?.ToString());
            return new Person
            {
                Id = id,
                Name = worker.FullName,
                Department = Int64.Parse(worker.Department),
                IsActive = existingPerson?["isActive"].Value.ToString() != "" && Convert.ToBoolean(existingPerson?["isActive"].Value),               
                Location = Int64.Parse(worker.Region)
            };
        }

        public async Task SendData(List<Worker> workers)
        {
            var getUrl = String.Concat(RequestPath, "people");

            var persons = await EntityItemsManager.GetAll(getUrl);

            getUrl = String.Concat(RequestPath, "location"); 
            var locations = await EntityItemsManager.GetAll(getUrl);

            getUrl = String.Concat(RequestPath, "department");
            var departments = await EntityItemsManager.GetAll(getUrl);

            ChangeRelationFields(workers, persons, locations, departments);

            foreach (var worker in workers)
            {
                var person = FormPersonToSend(worker, persons);
                if (worker.Id != null)
                {
                    var putUrl = String.Concat(RequestPath, "people/", worker.Id);
                    await new RequestSender<Person>().Put(putUrl, person);                       
                }
                else
                {
                    var postUrl = String.Concat(RequestPath, "people");
                    await new RequestSender<Person>().Post(postUrl, person);
                }
            }
        }

    }
}
