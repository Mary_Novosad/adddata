﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AnalyticToolAddData.Models;
using AnalyticToolAddData.Models.DTOs;
using Skill = AnalyticToolAddData.Models.DTOs.Skill;

namespace AnalyticToolAddData.DataTransfer
{
    public class SkillManager
    {
        private const string RequestPath = "http://localhost:55569/api/data/";
        public async Task SendData(ICollection<Models.Skill> skills)
        {
            var getUrl = Path.Combine(RequestPath, "Skill");
            var allSkills = await EntityItemsManager.GetAll(getUrl);

            getUrl = Path.Combine(RequestPath, "SkillType"); 
            var allSkillTypes = await EntityItemsManager.GetAll(getUrl);

            foreach (var skill in skills)
            {
                var skillDto = FormSkillToSend(skill, allSkills, allSkillTypes);
                if (skillDto.Id != 0)
                {
                    var putUrl = String.Concat(RequestPath, "Skill/", skillDto.Id);
                    await new RequestSender<Skill>().Put(putUrl, skillDto);
                }
                else
                {
                    var postUrl = String.Concat(RequestPath, "Skill");
                    await new RequestSender<Skill>().Post(postUrl, skillDto);
                }
            }
        }

        private Skill FormSkillToSend(Models.Skill skill, EntityItems skills, EntityItems skillTypes)
        {
            EntityItem entitySkill = skills.Items.FirstOrDefault(item => item["name"].Value.ToString().Equals(skill.Name, StringComparison.InvariantCultureIgnoreCase));
            long id = 0;
            Int64.TryParse(entitySkill?["id"].Value.ToString(), out id);
            EntityItem skillType = skillTypes.Items.FirstOrDefault(item => item["name"].Value.ToString().Equals(String.Concat("Hard Skill_", skill.Type), StringComparison.InvariantCultureIgnoreCase));
            int fadeout = 0;
            Int32.TryParse(entitySkill?["fadeoutPeriod"].Value?.ToString(), out fadeout);

            var s = new Skill
            {
                Id = id,
                Name = skill.Name,
                SkillType = (long)skillType["id"].Value,
                Description = entitySkill?["description"].Value?.ToString() ?? "",
                FadeoutPeriod = fadeout,
                LearningSource = entitySkill?["learningSource"].Value?.ToString() ?? "",
                SuperiorSkill = (long?)entitySkill?["superiorSkill"].Value
            };
            return s;
        }
    }
}
