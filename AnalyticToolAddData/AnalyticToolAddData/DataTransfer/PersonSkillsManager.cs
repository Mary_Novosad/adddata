﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnalyticToolAddData.Models;
using AnalyticToolAddData.Models.DTOs;

namespace AnalyticToolAddData.DataTransfer
{
    public class PersonSkillsManager
    {
        private const string RequestPath = "http://localhost:55569/api/data/";
        public async Task SendData(ICollection<WorkerSkills> workersSkills)
        {
            var getUrl = String.Concat(RequestPath, "People");
            var people = await EntityItemsManager.GetAll(getUrl).ConfigureAwait(false);

            getUrl = String.Concat(RequestPath, "Skill");
            var skills = await EntityItemsManager.GetAll(getUrl).ConfigureAwait(false);

            getUrl = String.Concat(RequestPath, "PeopleSkill"); 
            var peopleSkills = await EntityItemsManager.GetAll(getUrl).ConfigureAwait(false);

            foreach (var workerSkills in workersSkills)
            {
                var requestSender = new RequestSender<PersonSkill>();
                var personSkills = await FormSkillToSend(workerSkills, people, skills, peopleSkills);
                foreach (var workerSkill in personSkills)
                {
                    if (workerSkill.Level != 0)
                    {
                        if (workerSkill.Id != 0)
                        {
                            var putUrl = String.Concat(RequestPath, "PeopleSkill/", workerSkill.Id);
                            await requestSender.Put(putUrl, workerSkill);
                        }
                        else
                        {
                            var postUrl = String.Concat(RequestPath, "PeopleSkill");
                            await requestSender.Post(postUrl, workerSkill);
                        }
                    }
                }
            }
        }

        private async Task<ICollection<PersonSkill>> FormSkillToSend(WorkerSkills workerSkills, EntityItems people, EntityItems skills, EntityItems peopleSkills)
        {
            var personSkills = new List<PersonSkill>();
            var person = people.Items.FirstOrDefault(x => x["name"].Value.ToString()
                                .Equals(workerSkills.Name, StringComparison.InvariantCultureIgnoreCase));
            if (person == null)
            {
                var postUrl = String.Concat(RequestPath, "People");

                await new RequestSender<Person>().Post(postUrl, new Person
                {
                    Id = 0,
                    Name =  workerSkills.Name,
                    IsActive = false,
                    Department = 3,
                    Location = 2
                });
            }
            people = await EntityItemsManager.GetAll(String.Concat(RequestPath, "People"));
            person = people.Items.FirstOrDefault(x => x["name"].Value.ToString()
                                .Equals(workerSkills.Name, StringComparison.InvariantCultureIgnoreCase));

            foreach (var skill in workerSkills.Skills)
            {               
                var personSkill = skills.Items.FirstOrDefault(x => x["name"].Value.ToString()
                                .Equals(skill.Key.Name, StringComparison.InvariantCultureIgnoreCase));

                var peopleSkill = peopleSkills.Items.FirstOrDefault(x => (long) x["people"].Value == (long) person["id"].Value
                                                                    && (long) x["skill"].Value == (long) personSkill["id"].Value);
                long id = 0;
                Int64.TryParse(peopleSkill?["id"].Value.ToString(), out id);
                var result = new PersonSkill
                {
                    Id = id,
                    People = (long) person["id"].Value,
                    Skill = (long) personSkill["id"].Value,                    
                    Level = skill.Value
                };
                personSkills.Add(result);
            }
            return personSkills;
        }
    }
}
