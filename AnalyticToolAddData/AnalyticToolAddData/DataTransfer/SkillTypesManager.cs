﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AnalyticToolAddData.Models;
using AnalyticToolAddData.Models.DTOs;
using Skill = AnalyticToolAddData.Models.Skill;

namespace AnalyticToolAddData.DataTransfer
{
    public class SkillTypeManager
    {
        private const string RequestPath = "http://localhost:55569/api/data/SkillType/";
        public async Task SendData(ICollection<Skill> skills)
        {
            //var getUrl = Path.Combine(RequestPath, "SkillType");
            var skillTypes = await EntityItemsManager.GetAll(RequestPath);

            foreach (var skill in skills)
            {
                var skillType = FormSkillTypeToSend(skill, skillTypes);
                if (skillType.Id != 0)
                {
                    var putUrl = String.Concat(RequestPath, skillType.Id);
                    await new RequestSender<SkillType>().Put(putUrl, skillType);
                }
                else
                {                   
                    await new RequestSender<SkillType>().Post(RequestPath, skillType);
                }
            }
        }

        private SkillType FormSkillTypeToSend(Skill skill, EntityItems skillTypes)
        {
            EntityItem skillType = skillTypes.Items.FirstOrDefault(item => item["name"].Value.ToString().Equals(String.Concat("Hard Skill_", skill.Type), StringComparison.InvariantCultureIgnoreCase));
            long id = 0;
            Int64.TryParse(skillType?["id"].Value.ToString(), out id);
            return new SkillType()
            {
                Id = id,
                Name = String.Concat("Hard Skill_",  skill.Type)
            };
        }
    }
}
