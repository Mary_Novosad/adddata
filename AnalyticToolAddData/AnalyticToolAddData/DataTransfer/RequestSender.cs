﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace AnalyticToolAddData.DataTransfer
{
    public class RequestSender<TEntity> : IDisposable
    {
        private const string ApplicationJson = "application/json";
        private readonly HttpClient _client = new HttpClient();

        public Task<HttpResponseMessage> Put(string url, TEntity entity)
        {
            var httpContent = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, ApplicationJson);
            return _client.PutAsync(url, httpContent);
        }

        public Task<HttpResponseMessage> Post(string url, TEntity entity)
        {
            var httpContent = new StringContent(JsonConvert.SerializeObject(entity), Encoding.UTF8, ApplicationJson);
            return _client.PostAsync(url, httpContent);
        }

        public void Dispose()
        {
            _client?.Dispose();
        }
    }
}
