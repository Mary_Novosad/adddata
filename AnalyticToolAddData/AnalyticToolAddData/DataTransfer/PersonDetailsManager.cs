﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AnalyticToolAddData.Models;
using AnalyticToolAddData.Models.DTOs;
using Newtonsoft.Json;

namespace AnalyticToolAddData.DataTransfer
{
    public class PersonDetailsManager
    {
        private const string RequestPath = "http://localhost:55569/api/data/";
        public async Task SendData(ICollection<Worker> workers)
        {
            var getUrl = Path.Combine(RequestPath, "PeopleDetail");
            var workersDetails = await EntityItemsManager.GetAll(getUrl);

            getUrl = Path.Combine(RequestPath, "People");
            var people = await EntityItemsManager.GetAll(getUrl);

            foreach (var worker in workers)
            {
                var personSkills = FormPersonDetailsToSend(worker, workersDetails, people);
                string json = JsonConvert.SerializeObject(personSkills);
                if (personSkills.Id != 0)
                {
                    var putUrl = String.Concat(RequestPath, "PeopleDetail/", personSkills.Id);
                    await new RequestSender<PersonDetails>().Put(putUrl, personSkills);
                }
                else
                {
                    var postUrl = String.Concat(RequestPath, "PeopleDetail");

                    await new RequestSender<PersonDetails>().Post(postUrl, personSkills);
                }
            }
        }

        private PersonDetails FormPersonDetailsToSend(Worker worker, EntityItems workersDetails, EntityItems people)
        {
            EntityItem workerDetails = workersDetails.Items.FirstOrDefault(item => item["login"].Value.ToString().Equals(worker.FullName, StringComparison.InvariantCultureIgnoreCase));
            long id = 0;
            Int64.TryParse(workerDetails?["id"].Value.ToString(), out id);

            EntityItem person = people.Items.FirstOrDefault(item => item["name"].Value.ToString().Equals(worker.FullName, StringComparison.CurrentCultureIgnoreCase));

            return  new PersonDetails
            {
                Id = id,
                Email = worker.EleksEmail,
                EndDate = workerDetails?["endDate"]?.Value.ToString() != "" ? (DateTime?)workerDetails?["endDate"]?.Value : null,
                StartDate = workerDetails?["startDate"]?.Value.ToString() != "" ? (DateTime?)workerDetails?["startDate"]?.Value : null,
                EnglishLevel = worker.EnglishLevel,
                JobTitle = worker.JobTitle,
                Login = worker.FullName,
                People = (long) person["id"].Value,
                Phone = worker.Cellphone,
                Skype = worker.SkypeMessenger,
                TechnicalLevel = workerDetails?["technicalLevel"]?.Value.ToString() ?? ""
            };
        }
    }
}
