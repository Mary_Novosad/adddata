﻿using System.Net.Http;
using System.Threading.Tasks;
using AnalyticToolAddData.Models;
using Newtonsoft.Json;

namespace AnalyticToolAddData.DataTransfer
{
    public class EntityItemsManager
    {
        public static async Task<EntityItems> GetAll(string url)
        {
            var entityItems = new EntityItems();
            using (var client = new HttpClient())
            {                
                using (var response = await client.GetAsync(url))
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    entityItems = JsonConvert.DeserializeObject<EntityItems>(content);
                }
            }
            return entityItems;
        }
    }
}
